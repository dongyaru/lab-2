#include <stdio.h>
#include <stdlib.h>
#include<mpi.h> 
#define MAX 100


//auto refesh
void highlifeKernal(int rank,int procs,int size,int *hf,int *tmp,int argc, char** argv)
{
	
	int ii,jj,t;
	int count;
	for(int ii=0;ii<size;ii++){
		for(int jj=0;jj<size;jj++){
			tmp[ii*size+jj]=hf[ii*size+jj];
		}
	}
	MPI_Bcast(tmp,size*size,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&size,1,MPI_INT,0,MPI_COMM_WORLD);
for(t=0;t<MAX;t++)
	{	
	for(ii=1;ii<size-1;ii++)
	{
		for(jj=1;jj<size-1;jj++)//
		{
			if(size-rank+ii*procs<=jj){	
			count =tmp[(ii-1)*size+jj-1]+tmp[(ii-1)*size+jj]+tmp[(ii-1)*size+jj+1]
			+tmp[ii*size+jj-1] +tmp[ii*size+jj+1]
			+tmp[(ii+1)*size+jj-1]+tmp[(ii+1)*size+jj]+tmp[(ii+1)*size+jj+1];
			}
			else
			{
			count =tmp[(ii-1)*size+jj-1]+tmp[(ii-1)*size+jj]+tmp[(ii-1)*size+jj+1]
			+tmp[ii*size+jj-1] +tmp[ii*size+jj+1]
			+tmp[(ii+1)*size+jj-1]+tmp[(ii+1)*size+jj]+tmp[(ii+1)*size+jj+1];
			}
			if(count ==2 || count ==3 )
				hf[ii*size+jj]=1;
			else if(count ==6 )
				hf[ii*size+jj]=tmp[ii*size+jj];
			else
				hf[ii*size+jj]=0;
			
				
		}
		}
	}
	MPI_Bcast(hf,size*size,MPI_INT,0,MPI_COMM_WORLD);	
}		

int main(int argc, char** argv)
{
	int size;
	int i,j;
	if(argc>1){
		size=strtol(argv[1],NULL,16);
	}
	
	int *hf=(int*)malloc(size*size*sizeof(int));
	int *hh=(int*)malloc(size*size*sizeof(int));
	int rank = 0, procs = 0;
	MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Status status; 
	for(i=0;i<size;i++)
	{
		for(j=0;j<size;j++)
		{
			hf[i*size+j]=rand()%2; 
		}
	}
	
	
		
	highlifeKernal(rank,procs,size,hf,hh,argc,argv);
	
	
	if(rank==0){
	for(i=0;i<size;i++)
	{
		for(j=0;j<size;j++)
		{
			if(hf[i*size+j]==1){
				printf(" ▲ ");
			}
			else{
				printf(" O ");
			}
		}
		printf("\n");
	
	}
	}
	free(hf);
	free(hh);
	MPI_Finalize();
	//
	return 0;
}
